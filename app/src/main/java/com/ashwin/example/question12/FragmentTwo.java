package com.ashwin.example.question12;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Ashwin on 05-09-2017.
 */

public class FragmentTwo extends Fragment {

    private View mView;
    private TextView mTextView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_two, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mView = view;
        initViews();
    }

    private void initViews() {
        mTextView = (TextView) mView.findViewById(R.id.textView);
    }

    public void updateTextValue(CharSequence newText) {
        if (mTextView != null) {
            mTextView.setText(newText);
        }
    }

}
