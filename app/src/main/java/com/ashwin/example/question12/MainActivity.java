package com.ashwin.example.question12;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    FragmentOne mFragmentOne;
    FragmentTwo mFragmentTwo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
    }

    private void initViews() {
        showFragmentOne();
        showFragmentTwo();

        mFragmentOne.setTextChangeListener(new FragmentOne.TextChangeListener() {
            @Override
            public void onTextChange(CharSequence text) {
                if (mFragmentTwo != null) {
                    mFragmentTwo.updateTextValue(text);
                }
            }
        });
    }

    // Fragment 1
    private void showFragmentOne() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        mFragmentOne = new FragmentOne();
        fragmentTransaction.replace(R.id.fragmentOneLayout, mFragmentOne);
        fragmentTransaction.commit();
    }

    // Fragment 2
    private void showFragmentTwo() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        mFragmentTwo = new FragmentTwo();
        fragmentTransaction.replace(R.id.fragmentTwoLayout, mFragmentTwo);
        fragmentTransaction.commit();
    }
}
